﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Remotion.Linq.Parsing.ExpressionVisitors.Transformation.PredefinedTransformations;

namespace ToWeb.WebSocket.Server.Controllers
{
    [Route("api/[controller]")]
    public class ChatHub : Hub
    {
        private static List<ChatMessage> _messages = new List<ChatMessage>();

        public Task SendToAll(string nick, string message)
        {
            _messages.Add(new ChatMessage() { Nick = nick, Message = message  });

            List<string> connectionIdToIgnore = new List<string> {Context.ConnectionId};
            return Clients.AllExcept(connectionIdToIgnore).SendAsync("sendToAll", nick, message);
        }

        public Task GetMessages()
        {
            return Clients.Client(Context.ConnectionId)
                .SendAsync("sendToAll", _messages.Skip(Math.Max(0, _messages.Count() - 10)));
        }
    }

    public class ChatMessage
    {
        public string Nick { get; set; }
        public string Message { get; set; }
    }
}
