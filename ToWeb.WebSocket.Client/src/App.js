import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { HubConnection } from '@aspnet/signalr/dist/browser/signalr.min.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { messages: [], message: '', nick: '', hubConnection: null };
  }

  componentDidMount() {
    const nick = 'Johan';
    const hubConnection = new HubConnection('http://localhost:1333/chat');

    this.setState({ hubConnection, nick }, () => {
      this.state.hubConnection
        .start({ withCredentials: false })
        .then(() => console.log('Connection started!'))
        .catch(err => console.log('Error while establishing connection', err));

      this.state.hubConnection.on('sendToAll', (nick, receivedMessage) => {        
        const messages = this.state.messages.concat([`${nick}: ${receivedMessage}`]);
        this.setState({ messages: messages });
      });
    });
  }

  sendMessage = () => {
    this.state.hubConnection
      .invoke('sendToAll', this.state.nick, this.state.message)
      .catch(err => console.error(err));    
    
    const messages = this.state.messages.concat([`ME: ${this.state.message}`]);
    this.setState({ messages: messages, message: '' });
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div>
          <input type="text" value={this.state.message} onChange={e => this.setState({ message: e.target.value })} />
          <button onClick={this.sendMessage}>Send</button>
        </div>
        <div>
          {this.state.messages.map((m, i) => {
            return (<div key={i}>{m}</div>)
          })}
        </div>
      </div>
    );
  }
}

export default App;
